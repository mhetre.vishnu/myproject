﻿namespace CRUD_API_Baxture.Models
{
    public class UsersModel
    {
        public int UserId { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public bool IsAdmin { get; set; }
        public string? Hobbies { get; set; }
    }
            
    public class UsersModelSelectByRole
    {
        //public bool IsAdmin { get; set; }
        public int UserId { get; set; }
    }

}
