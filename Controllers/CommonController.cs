﻿using CRUD_API_Baxture.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CRUD_API_Baxture.Controllers
{
    [Route("[controller]")]
    [Produces("application/json")]

    public class CommonController : ControllerBase
    {
        string connectionString = "Server=(localdb)\\localDB1;Database=tempdb;Trusted_Connection=True;";

        List<UsersModel> usersList = new List<UsersModel>();

        [Route("GetUsers")]
        [HttpGet()]
        public JsonResult getUsers(UsersModelSelectByRole usersModelSelectByRole)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("[dbo].[getUsers_SelectById]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@UserId", usersModelSelectByRole.UserId);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //if (!reader.HasRows)
                            //{

                            //}
                            while (reader.Read())
                            {
                                UsersModel user = new UsersModel
                                {
                                    UserId = Convert.ToInt32(reader["Id"]),
                                    Username = reader["Username"].ToString(),
                                    Password = reader["Password"].ToString(),
                                    IsAdmin = reader["IsAdmin"] != DBNull.Value ? Convert.ToBoolean(reader["IsAdmin"]) : false,
                                    Hobbies = reader["Hobbies"] != DBNull.Value ? reader["Hobbies"].ToString() : string.Empty
                                };
                                // Add the UsersModel object to the list
                                usersList.Add(user);
                            }
                        }
                    }
                }

                return new JsonResult(usersList);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return new JsonResult(500, "Internal Server Error");
            }
        }
    }
}
